import 'package:flutter/material.dart';
import 'package:splash_test/splashscreen_view.dart';

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Splash Hello',
      home: SplashScreenPage()));
}
